<h1>Package Enssop/Formcontact</h1>
<span>Pour intaller le paquet : </span>
<ol>
    <li>Faire un composer require enssop/formcontact</li> 
    <li>Pour un projet Laravel rajouter cette ligne dans le fichier config/app.php : Enssop\FormContact\FormContactServiceProvider::class,</li>  
    <li>Pour accéder au formulaire de contact : www.nomdevotreici.fr/contact </li> 
    <li>php artisan vendor:publish --provider="Enssop\Administration\FormContactServiceProvider"</li> 
    <li>Mettre les seeders dans le seeder principale</li> 
    
        
</ol>