<?php

namespace Enssop\FormContact;

use Illuminate\Support\ServiceProvider;

class FormContactServiceProvider extends ServiceProvider
{
    public function register()
    {
        // register our controller
        $this->app->make('Enssop\FormContact\FormContactController');

        // register our views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'FormContact');

        $this->app->bind(
            'Enssop\FormContact\App\Gestion\FormContactMailGestionInterface',
            'Enssop\FormContact\App\Gestion\FormContactMailGestion'
        );
    }
    public function boot()
    {

        include __DIR__ . '/routes.php';
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->publishes([
            __DIR__.'/resources/asset' => public_path('enssop/FormContact')
        ], 'public');

        $this->publishes([
            __DIR__.'/database/migrations' => database_path('migrations'),
        ], 'migrations');

        $this->publishes([
            __DIR__.'/database/seeds' => database_path('seeds'),
        ], 'seeds');


        $this->app['router'];
    }
}
