<?php

namespace Enssop\FormContact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Enssop\FormContact\App\Gestion\FormContactMailGestionInterface;

class FormContactController extends Controller
{

    public function index()
    {
        return view('FormContact::home');
    }

    public function create()
    {
        //
    }

    public function store(Request $request, FormContactMailGestionInterface $reponsemailgestion)
    {
            // Condition si l'envoi de mail vien du panel administration ou du formulaire
            if ($request->tokenmail == "confirm" || $request->tokenmail == 'refus') {
                $reponsemailgestion->ReplyMailGestion($request);
                return redirect('admin/formcontactadmin');
            }else {
                // On passe par une interface puis une injection de dépendance
                $result = $reponsemailgestion->FormContactStorageMailGestion($request);

                //Si le mail est conforme on renverra une page de validation et l'utilisateur reçevera un mail sinon une page d'erreur
                if ($result == TRUE){
                    return view('FormContact::sendmail');
                }else{
                    return view('FormContact::sendMailError');
                }
            }
    }

    public function show()
    {

    }

    public function edit(Request $request, $tokenviewmail)
    {

    }

    public function update(Request $request, $id, $email, $tokenmail)
    {

    }

    public function destroy($id)
    {
    }
}

