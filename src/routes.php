<?php

Route::resource('contact', 'Enssop\FormContact\FormContactController');

Route::prefix('admin')->group(function () {
        Route::get('/formcontactconfirm/{id}/{mail}/{token}', function ($id, $mail, $token) {
            return view('FormContact::administration/formcontactconfirm')->with(['id' => $id, 'email' => $mail, 'token' => $token]);
        });
        Route::get('/formcontactrefus/{id}/{mail}/{token}', function ($id, $mail, $token) {
            return view('FormContact::administration/formcontactrefus')->with(['id' => $id, 'email' => $mail, 'token' => $token]);
        });
});

Route::get('/formacontact', function () {
    echo 'Ceci est la page pour Enssop/FormContact ';
});
