var app = new Vue({
    el: '#app',
    data: {
        page: 1,
    },
    computed: {
        checkPage: function () {
            
            if (this.page < 1 || this.page > 2){ this.page = 1 };

        }
    }
});