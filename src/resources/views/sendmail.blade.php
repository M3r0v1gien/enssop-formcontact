@extends('FormContact::layouts.master')

@section('content')
<div id="app">


    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('storage/Logo/Logo 03.png') }}" width="100" class="d-inline-block align-top"
                alt="{{ config('app.name') }}">
        </a>
    </nav>

    <div class="container h-50">
        <div class="m-5 jumbotron-fluid">
            <div class="container text-primary">
                <h1 class="display-4">{!! config('voirie.name') !!}</h1>
                <p class="lead">Demande d'arrêté de police de la circulation.</p>
            </div>
        </div>


        <div class="card border-success mb-3">
            <div class="card-header text-center">Votre Mail à bien été envoyer.</div>
            <div class="card-body text-success m-5">
                <h5 class="card-title">Demande Envoyer.</h5>
                <p class="card-text">
                    Vous allez recevoir un mail de confirmation de l'envoie de votre demande.
                </p>
            </div>
        </div>
        <div class="bg-transparent text-center">
            <a href="{{ url('contact') }}" class="btn btn-primary">Retour</a>
        </div>

    </div>
</div>
@endsection
