<!DOCTYPE html>
<html lang="fr">

<head>
    <title></title>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        * {
            box-sizing: border-box;
        }

        html,
        body {
            margin: 0;
            height: 100vh;
        }

        .content {
            margin-right: 15%;
            margin-left: 15%;
        }

        .text-center {
            text-align: center;
        }

        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, .125);
            border-radius: .25rem;
        }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
            width: 100%;
        }

        .card-footer {
            padding: 0.75rem 1.25rem;
            background-color: rgba(0, 0, 0, 0.03);
            border-left: 1px solid rgba(0, 0, 0, 0.125);
            border-right: 1px solid rgba(0, 0, 0, 0.125);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
            width: 100%;
            border-radius: .25rem;
        }

        .card-footer:last-child {
            border-radius: 0 0 calc(0.25rem - 1px) calc(0.25rem - 1px);
        }

        .lead {
            font-size: 1rem;
            margin-top: -.375rem;
            margin-bottom: 0;
            font-weight: 500;
            line-height: 1.2;
            color: #6c757d !important;
        }

    </style>
</head>

<body>
    <div class="content">
        <div class="text-center">
            <h1>{{ config('app.name') }}</h1>
        </div>
        <div class="card">
            <div class="card-body">
{{--                <h3 class="text-center">{!! config('voirie.name') !!}</h3>--}}
                <h6 class="text-center lead">Demande d'arrêté de police de la circulation.</h6>
                <p class="text-center">{{ $messageRefus }}</p>
            </div>
        </div>
        <div>
            <div class="card-footer">
                <h6 class="text-center lead">Merci de ne pas répondre à ce mail.</h6>
            </div>
        </div>
    </div>
</body>

</html>
