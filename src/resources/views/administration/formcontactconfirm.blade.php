@extends('Administration::layouts.master')
@extends('Administration::layouts.nav')
@extends('Administration::layouts.navmobile')

@section('content')


<div class="alert alert-info alert-with-icon" data-notify="container">
    <i class="material-icons" data-notify="icon">info</i>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
    </button>
    <span data-notify="message">
        Si vous laissez le message vide ci-dessus cela va envoyer un message par défaut
    </span>
</div>
<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Validation de la demande</h4>
        <p class="card-category">Si vous laissez le message vide ci-dessus cela vas
            envoyer un message par default</p>
    </div>
    <div class="card-body">
        <form action="{{ route('contact.store') }}" method="POST">
            @csrf
            @method('POST')
            <input type="hidden" name="id" value="{{ $id }}">
            <input type="hidden" name="email" value="{{ $email }}">
            <input type="hidden" name="tokenmail" value="{{ $token }}">
            <div class="form-group bmd-form-group mt-5">
                <label class="bmd-label-floating"> Votre Message :</label>
                <textarea class="form-control" rows="5" name="message" maxlength="255"></textarea>
            </div>
            <button type="submit" class="btn btn-primary pull-right my-4">Envoyer la confirmation de la demande</button>
        </form>
    </div>
</div>



@endsection