@extends('Administration::layouts.master')
@extends('Administration::layouts.nav')
@extends('Administration::layouts.navmobile')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Liste des demandes en attentes</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive-xl">
                    <table class="table table-striped">
                        <thead class="text-dark">
                        <tr>
                            <th class="text-center" scope="col">Numéro de la demande</th>
                            <th class="text-center" scope="col">Prénom</th>
                            <th class="text-center" scope="col">Nom de famille</th>
                            <th class="text-center" scope="col">Email</th>
                            <th class="text-center" scope="col">Document</th>
                            <th class="text-center" scope="col">Reçu</th>
                            <th class="text-center" scope="col">Réponse</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listMails as $listMail)
                            @if ($listMail->enable == true)
                                <tr>
                                    <td class="text-center">{{ $listMail->id }}</td>
                                    <td class="text-center text-capitalize">{{ $listMail->firstname }}</td>
                                    <td class="text-center text-capitalize">{{ $listMail->lastname }}</td>
                                    <td class="text-center">{{ $listMail->email }}</td>
                                    <td class="text-center">
                                        <a href="{{ asset ('storage/FormContact/'.$listMail->pathDoc) }}"
                                           target=_blank>{{ $listMail->pathDoc }}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        {{ Carbon\Carbon::parse($listMail->created_at)->format('\L\e d/m/Y \à H:i') }}</td>
                                    <td class="text-center text-center ">
                                        <div class="btn-group" role="group">
                                            <a class="btn btn-primary"
                                               href="{{ url('/admin/formcontactconfirm/'.$listMail->id.'/'.$listMail->email.'/'.$tokenmail = 'confirm' ) }}">
                                               Valider
                                            </a>
                                            <a class="btn btn-danger"
                                               href="{{ url('/admin/formcontactrefus/'.$listMail->id.'/'.$listMail->email.'/'.$tokenmail = 'refus' ) }}">
                                               Refusez
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<hr class="my-5 w-75 bg-secondary">

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Liste des demandes validées</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive-xl">
                    <table class="table table-striped">
                        <thead class="text-dark">
                        <tr>
                            <th class="text-center" scope="col">Numéro de la demande</th>
                            <th class="text-center" scope="col">Prénom</th>
                            <th class="text-center" scope="col">Nom de famille</th>
                            <th class="text-center" scope="col">Email</th>
                            <th class="text-center" scope="col">Document</th>
                            <th class="text-center" scope="col">Terminer</th>
                            <th class="text-center" scope="col">Réponse</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listMails as $listMail)
                            @if ($listMail->enable == false)
                                <tr>
                                    <td class="text-center">{{ $listMail->id }}</td>
                                    <td class="text-center text-capitalize">{{ $listMail->firstname }}</td>
                                    <td class="text-center text-capitalize">{{ $listMail->lastname }}</td>
                                    <td class="text-center">{{ $listMail->email }}</td>
                                    <td class="text-center">
                                        <a class="text-muted" href="{{ asset ('storage/FormContact/'.$listMail->pathDoc) }}"
                                           target=_blank>{{ $listMail->pathDoc }}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        {{ Carbon\Carbon::parse($listMail->updated_at)->format('\L\e d/m/Y \à H:i') }}</td>
                                    <td class="text-center text-success">Terminer</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
