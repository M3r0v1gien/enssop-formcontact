@extends('FormContact::layouts.master')

@section('content')
    <div id="app">

        {{-- Navbar --}}
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="https://placehold.jp/100x50.png" width="100" class="d-inline-block align-top" alt="">
            </a>
        </nav>
        {{-- /NavBar --}}


        {{-- Container --}}
        <div class="container h-50">

            {{-- header --}}
            <div class="m-5 jumbotron-fluid">
                <div class="container text-primary">
                    <h1 class="display-4">{!! config('voirie.name') !!}</h1>
                    <p >Demande d'arrêté de police de la circulation.</p>
                </div>
            </div>
            {{-- /header --}}

            {{-- nav --}}
            <ul class="nav nav-tabs">
                <li class="nav-item">
                <span style="cursor: pointer;" @click="page=1"
                      :class="[ page == 1 ? 'nav-link active' : 'nav-link' ]">Information</span>
                </li>
                <li class="nav-item">
                <span style="cursor: pointer;" @click="page=2"
                      :class="[ page == 2 ? 'nav-link active' : 'nav-link' ]">Réalisé ma demande</span>
                </li>
            </ul>
            {{-- /nav --}}

            {{-- div | page = 1  --}}
            <div :class="[ page == 1 ? 'd-block' : 'd-none' ]">
                <section class="mt-5 px-3 text-justify">
                    <h2>Lorem</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint similique esse molestias hic iusto,
                        illo
                        at saepe? Tempora, nesciunt, nemo, vitae consequuntur eos necessitatibus maiores fugit distinctio
                        accusantium commodi ut!</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint similique esse molestias hic iusto,
                        illo
                        at saepe? Tempora, nesciunt, nemo, vitae consequuntur eos necessitatibus maiores fugit distinctio
                        accusantium commodi ut!</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint similique esse molestias hic iusto,
                        illo
                        at saepe? Tempora, nesciunt, nemo, vitae consequuntur eos necessitatibus maiores fugit distinctio
                        accusantium commodi ut!</p>
                </section>
            </div>
            {{-- /div  --}}

            {{-- div | page = 2  --}}
            <div :class="[ page == 2 ? 'd-block' : 'd-none' ]">
                <section class="mt-5 px-3 text-justify">
                    <p class="d-flex align-items-center justify-content-center m-5 text-center">
                        Pour réaliser votre demande d'arrêter de voirie, merci de télécharger le formulaire ensuite une fois
                        rempli, <br> vous n'avez plus cas nous l'envoyer via le formulaire de contact ci-dessous.
                    </p>

                    <div class="d-flex align-items-center justify-content-center my-5">
                        <a class="btn btn-primary p-3"
                           href="https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14024.do" target="_blank">
                            Accéder au Formulaire
                        </a>
                    </div>
                </section>

                <hr class="w-50 bg-primary">

                <div class="container p-5">
                    <form action="{{ route("contact.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('POST')

                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="prenom">Prénom <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="prenom" name="fname" placeholder="Prénom"
                                           required>
                                </div>
                                <div class="col">
                                    <label for="nom">Nom <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="nom" name="lname" placeholder="Nom"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact">Email de Contact <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="contact" name="email"
                                   placeholder="name@example.com" required>
                        </div>
                        <div class="form-group">
                            <label for="contact">Ajouter le Formulaire <span
                                    class="text-danger">*</span></label>
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" id="customFileLang"
                                       lang="{{ str_replace('_', '-', app()->getLocale()) }}" accept="application/pdf"
                                       required>
                                <label class="custom-file-label" for="customFileLang">
                                    Sélectionner un fichier
                                </label>
                            </div>
                        </div>
                        <p><span class="lead text-danger">*</span> : Obligatoire</p>
                        <button type="submit" class="btn btn-primary btn-block">Envoyer</button>
                    </form>
                </div>
            </div>
            {{-- /div  --}}


        </div>
        {{-- /Container --}}

    </div>

    </div>
@endsection
