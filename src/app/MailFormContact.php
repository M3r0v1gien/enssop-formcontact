<?php

namespace Enssop\FormContact\App;

use Illuminate\Database\Eloquent\Model;

class MailFormContact extends Model
{
    protected $fillable = [];
    protected $table = 'mails';
}
