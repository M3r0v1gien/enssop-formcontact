<?php


namespace Enssop\FormContact\App\Gestion;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use Enssop\FormContact\App\MailFormContact;
use Enssop\FormContact\Emails\refusMailFormContact;
use Enssop\FormContact\Emails\reponseMailFormContact;
use Enssop\FormContact\Emails\sendMailFormContact;


class FormContactMailGestion implements FormContactMailGestionInterface
{
    public function ReplyMailGestion($request){
        if ($request->tokenmail == 'confirm'){
            $order = $request;
            $order->message = $order->message ?? "Votre demande d'arrêter de voirie à bien été confirmer. 
                                                Merci de passer à la Police Nationnal pour récupérer votre demande.";
            Mail::to($order->email)->send(new reponseMailFormContact($order));
            DB::table('mails')
                ->where('id', $order->id)
                ->update(['enable' => false, 'updated_at' => date('Y-m-d H:i:s', time())]);
        }else{
            $order = $request;
            $order->message = $order->message ?? "Votre demande d'arrêter de voirie à été Refusé.";
            Mail::to($order->email)->send(new refusMailFormContact($order));
            DB::table('mails')
                ->where('id', $order->id)
                ->update(['enable' => false, 'updated_at' => date('Y-m-d H:i:s', time()) ]);
        }

    }
    public function FormContactStorageMailGestion($request){

    // On récupère toutes les informations concernant le fichier que l'on reçoit
    $nameImg = $request->file->getClientOriginalName();
    $mimeType = $request->file->getMimeType();
    $urlImg = 'storage/FormContact';

    // Regex pour test si la fin de fichier est bien en PDF
    $modiffReg = '%.pdf$%';

    // On test si le format que l'on reçoit est bien un PDF grâce a une regrex et au type mime
    if ($mimeType == 'application/pdf' && preg_match($modiffReg, $nameImg)) {
        $file = $request->file('file');
        $file->move($urlImg, $nameImg);
        $order = $request;

        // On utilise un model pour faire l'insertion en Base de Données
        Mail::to($request->email)->send(new sendMailFormContact($order));
        $mail = new MailFormContact();
        $mail->firstname = $request->fname;
        $mail->lastname = $request->lname;
        $mail->email = $request->email;
        $mail->pathDoc = $nameImg;
        $mail->enable = true;
        $mail->save();
        return 1 ;
    }
}

}
