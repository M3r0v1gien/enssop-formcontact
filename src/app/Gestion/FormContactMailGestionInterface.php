<?php

namespace Enssop\FormContact\App\Gestion;


interface FormContactMailGestionInterface{

    public function ReplyMailGestion($request);
    public function FormContactStorageMailGestion($request);

}
