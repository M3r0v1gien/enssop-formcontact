<?php

namespace Enssop\FormContact\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendMailFormContact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var FormContactOrder
     */

    public $FormContactOrder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $FormContactReceive)
    {

        $this->FormContactOrder = $FormContactReceive;
    }



    /**
     * Build the message.
     *

     */
    public function build()
    {
//        dd($this->FormContactOrder);
        $this->from(env('MAIL_USERNAME'))
            ->subject('Demande d\'arrêté de la circulation')
            ->view('FormContact::emails.confirm')
            ->with(['formContact'=>$this->FormContactOrder]);
    }
}
