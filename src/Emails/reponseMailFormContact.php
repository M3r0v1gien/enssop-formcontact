<?php

namespace Enssop\FormContact\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Ramsey\Uuid\Codec\OrderedTimeCodec;

class reponseMailFormContact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var FormContactOrder
     */

    public $FormContactOrder;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $FormContactReceive)
    {
        $this->FormContactOrder = $FormContactReceive;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(env('MAIL_USERNAME'))
            ->subject("Reponse à votre demande d'arrêté de la circulation")
            ->view('FormContact::emails.reponseEmail')->with(['messageConfir'=>$this->FormContactOrder]);
    }
}
