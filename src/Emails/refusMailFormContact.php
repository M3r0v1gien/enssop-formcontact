<?php

namespace Enssop\FormContact\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Ramsey\Uuid\Codec\OrderedTimeCodec;

class refusMailFormContact extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The order instance.
     *
     * @var FormContactOrder
     */

    public $FormContactOrder;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $FormContactReceive)
    {
//        dd($FormContactReceive->message);
        $this->FormContactOrder = $FormContactReceive;
        $this->message = $FormContactReceive->message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $this->from(env('MAIL_USERNAME'))
            ->subject("Refus de votre demande d'arrêté de la circulation")
            ->view('FormContact::emails.refus')->with(['messageRefus'=>$this->FormContactOrder->message]);
    }
}
