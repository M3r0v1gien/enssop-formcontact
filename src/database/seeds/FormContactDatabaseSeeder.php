<?php

namespace Enssop\FormContact\Database\Seeds;

use Illuminate\Database\Seeder;

class FormContactDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(FormContactSeeder::class);
    }
}
