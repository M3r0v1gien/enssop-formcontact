<?php

use Illuminate\Database\Seeder;

class FormContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            'name' => 'Contact',
            'route_home' => '/contact',
            'route_admin' => '/admin/formcontactadmin',
            'enable' => 1,
        ]);
    }
}
